<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use simplehtmldom\HtmlWeb;

class IndexController extends Controller
{
    public function index(Request $request){
        return view('index');
    }

    public function getResult(Request $request){
        if ( true ) {
            $domain = $request->get('domain');
            $url = 'https://'. $domain;
            if ( $domain ) {
                // Шаблон данных
                $data = [
                    'meta' => [
                        'title' => '',
                        'meta_tags' => [],
                    ],
                    'company_info' => [
                        'phone' => '',
                        'clickable_phone' => false,
                        'email' => '',
                        'clickable_email' => false,
                        'work_time' => '',
                        'address' => '',
                    ],
                    'headings' => [
                        'h1' => 0,
                        'h2' => 0,
                        'h3' => 0,
                        'h4' => 0,
                        'h5' => 0,
                        'h6' => 0,
                    ],
                    'beautiful_routing' => true,
                    'pages' => [
                        "payment" => false,
                        "delivery" => false,
                        "warranty" => false,
                        "return" => false,
                        "about_company" => false,
                        "promotions" => false,
                        "reviews" => false,
                        "certificates" => false,
                        "contacts" => false,
                        "prices" => false,
                    ],
                ];
                //ключевые слова
                $dictionary = [
                    [
                        "title" => "payment",
                        "keywords" => "(плат|куп)"
                    ],
                    [
                        "title" => "delivery",
                        "keywords" => "(доставка|транспортировка|отправка|поставка|прересылка)"
                    ],
                    [
                        "title" => "warranty",
                        "keywords" => "(гаранти)"
                    ],
                    [
                        "title" => "return",
                        "keywords" => "(возврат|сдач|возвращение)"
                    ],
                    [
                        "title" => "about_company",
                        "keywords" => "(о нас|компани|фирм|обществ|ассоциаци|организаци|объединени|предприя|проект|сайте)"
                    ],
                    [
                        "title" => "promotions",
                        "keywords" => "(акции|акция|купон|скидка|скидки|распродаж)"
                    ],
                    [
                        "title" => "reviews",
                        "keywords" => "(отзыв|комментари|оценк|впечатлен|реакци)"
                    ],
                    [
                        "title" => "certificates",
                        "keywords" => "(сертификат|патент|свидетельств|удостоверен)"
                    ],
                    [
                        "title" => "contacts",
                        "keywords" => "(контакт|связ|координац|общени|коммуникац|номер телефона|email|почта|социальные сети)"
                    ],
                    [
                        "title" => "prices",
                        "keywords" => "(цена|цены|стоимос|такса|прайс|расценка|расценки)"
                    ],
                ];
                // Получение данных индексной страницы
                $client = new HtmlWeb();
                $html = $client->load($url);
                if ( !$html ) {
                    $url = str_replace('https://','http://', $url);
                    $html = $client->load($url);
                    if ( !$html ) {
                        $result = [
                            'status' => -1,
                            'message' => 'Произошла ошибка при загрузке данных с введенного домена!',
                            'data' => [],
                        ];
                        return json_encode($result);
                    }
                }
                $metaTags = $html->find('meta');

                // Получение тега title индексной страницы
                $titleTag = $html->find('title');
                if ( $metaTags ) {
                    $data['meta']['title'] = $titleTag[0]->plaintext;
                }

                // Получение мета тегов индексной страницы
                if ($metaTags) {
                    foreach ($metaTags as $tag) {
                        if ( strtolower($tag->name) == 'keywords' || strtolower($tag->name) == 'description' ) {
                            $data['meta']['meta_tags'][$tag->name] = $tag->content;
                        }
                    }
                }

                // Получение тегов h
                $hTags = [];
                for($i = 1; $i <= 6; $i++) {
                    $hTags['h'.$i] = count($html->find('h'.$i));
                }
                foreach ( $hTags as $key => $value ) {
                    $data['headings'][$key] = $value;
                }


                $header_footer = '';
                if ( $html->find('header') ) {
                    $header_footer = $html->find('header')[0]->outertext;
                } elseif ( $html->find('.header') ) {
                    $header_footer = $html->find('.header')[0]->outertext;
                } elseif ( $html->find('#header') ) {
                    $header_footer = $html->find('#header')[0]->outertext;
                }
                if ( $html->find('footer') ) {
                    $header_footer .= $html->find('footer')[0]->outertext;
                } elseif ( $html->find('.footer') ) {
                    $header_footer .= $html->find('.footer')[0]->outertext;
                } elseif ( $html->find('#footer') ) {
                    $header_footer .= $html->find('#footer')[0]->outertext;
                }

                // Получение телефона
                if ( preg_match('/tel:(\+7|8)[0-9]+/', preg_replace('/(\s|\(|\)|-)/', '', $header_footer), $matches) ) {
                    $data['company_info']['phone'] = str_replace('tel:', '', $matches[0]);
                    $data['company_info']['clickable_phone'] = true;
                } else {
                    if (preg_match('/((\+7|8)([0-9]{10}|[0-9]{4}(-|\s)[0-9]{6}|[0-9]{3}(-|\s)[0-9]{3}(-|\s)[0-9]{2}(-|\s)[0-9]{2}))/', preg_replace('/(\s|\(|\)|-)/', '', $header_footer), $matches)) {
                        $data['company_info']['phone'] = $matches[0];
                    }
                }
                // Получение email
                if ( preg_match('/mailto:([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}/', $header_footer, $matches) ) {
                    $data['company_info']['clickable_email'] = true;
                    $data['company_info']['email'] = str_replace('mailto:', '', $matches[0]);
                } else {
                    if (preg_match('/([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}/', $header_footer, $matches)) {
                        $data['company_info']['email'] = $matches[0];
                    }
                }
                // Получение времени работы
                // По дням
                if (preg_match('/(пн|пон|понедельник|вт|втор|вторник|ср|сред|среда|чт|чет|четверг|пт|пят|пятница|сб|суб|суббота|вс|воск|воскресенье)(\s-\s|-)-(пн|пон|понедельник|вт|втор|вторник|ср|сред|среда|чт|чет|четверг|пт|пят|пятница|сб|суб|суббота|вс|воск|воскресенье)/', strtolower($header_footer), $matches)) {
                    $data['company_info']['work_time'] = $matches[0];
                }
                if (preg_match('/\d+:\d+(-|\s(по|до)\s)\d+:\d+/', preg_replace('/\s/', '', strtolower($header_footer)), $matches)) {
                    $data['company_info']['work_time'] .= $matches[0];
                }
                // Адрес

                $html_plaintext = [];
                if ( $html->find('header') ) {
                    $html_plaintext = $html->find('header')[0]->plaintext;
                } elseif ( $html->find('.header') ) {
                    $html_plaintext = $html->find('.header')[0]->plaintext;
                } elseif ( $html->find('#header') ) {
                    $html_plaintext = $html->find('#header')[0]->plaintext;
                }
                if ( $html->find('footer') ) {
                    $html_plaintext .= $html->find('footer')[0]->plaintext;
                } elseif ( $html->find('.footer') ) {
                    $html_plaintext .= $html->find('.footer')[0]->plaintext;
                } elseif ( $html->find('#footer') ) {
                    $html_plaintext .= $html->find('#footer')[0]->plaintext;
                }


                $vars = [];
                preg_match('/([0-9]{6})(((\,)(\s*))|\s|\s*)([г](((\.)(\s*))|\.)|город)([а-я]*)(((\,)(\s*))|\s|\s*)/ui',$html_plaintext,$matches);

                array_push($vars,$matches);
                preg_match('/(ул(((\.)(\s*))|\.)|улица)([а-я]*)/ui',$html_plaintext,$matches);
                array_push($vars,$matches);
                preg_match('/(д(((\.)(\s*))|\.)|дом)([0-9]*)/ui',$html_plaintext,$matches);
                array_push($vars,$matches);

                $address = '';
                $data['company_info']['address'] = $address;



                // Получение ссылок из подвала сайта
                $links = [];
                $metaStaticPages = [];
                $footerLinks = $html->find('footer a');
                if ( count($footerLinks) == 0 ) {
                    $footerLinks = $html->find('.footer a');
                }
                if ( count($footerLinks) == 0 ) {
                    $footerLinks = $html->find('#footer a');
                }
                foreach ($footerLinks as $link) {
                    if ( !preg_match("/{$domain}/", $link->href) ) {
                        $links[] = $url. $link->href;
                    } else {
                        $links[] = $link->href;
                    }
                    $page_client = new HtmlWeb();
                    $page_html = $page_client->load($links[count($links) - 1]);
                    if ( $page_html ) {
                        $title = $page_html->find('title');
                        $description = $page_html->find('meta[name="description"]');
                        if ( $title || $description ) {
                            $metaStaticPages[] = [
                                'title' => $page_html->find('title') ? $page_html->find('title')[0]->plaintext : null,
                                'description' => $page_html->find('meta[name=description]') ? $page_html->find('meta[name=description]')[0]->content : null,
                            ];
                        }
                    }
                }

                // Получение ссылок из шапки сайта
                $headerLinks = $html->find('header a');
                if ( count($headerLinks) == 0 ) {
                    $headerLinks = $html->find('.header a');
                }
                if ( count($headerLinks) == 0 ) {
                    $headerLinks = $html->find('#header a');
                }
                foreach ($headerLinks as $link) {
                    $skip = false;
                    foreach ($links as $l) {
                        if ( $l == $link->href ) {
                            $skip = true;
                        }
                    }
                    if ( $skip ) {
                        continue;
                    }
                    if ( !preg_match("/{$domain}/", $link->href) ) {
                        $links[] = $url. $link->href;
                    } else {
                        $links[] = $link->href;
                    }
                    $page_client = new HtmlWeb();
                    $page_html = $page_client->load($links[count($links) - 1]);
                    if ( $page_html ) {
                        $title = $page_html->find('title');
                        $description = $page_html->find('meta[name="description"]');
                        if ( $title || $description ) {
                            $metaStaticPages[] = [
                                'title' => $page_html->find('title') ? $page_html->find('title')[0]->plaintext : null,
                                'description' => $page_html->find('meta[name=description]') ? $page_html->find('meta[name=description]')[0]->content : null,
                            ];
                        }
                    }
                }
                // Убираем дублирующиеся ссылки
                $links = array_unique($links);

                // Проверяем ЧПУ ссылок
                foreach ($links as $link) {
                    if ( preg_match('/(-[0-9]{3,}+|[0-9]{3,}$)/', $link) ) {
                        $data['beautiful_routing'] = false;
                        break;
                    }
                }

                // Проверка meta-тегов у статичных страниц
                foreach($metaStaticPages as $metaStaticPage){
                    foreach($dictionary as $item){
                        if (preg_match("/{$item['keywords']}/",$metaStaticPage['title']) || preg_match("/{$item['keywords']}/",$metaStaticPage['description'])){
                            $data['pages'][$item['title']] = true;
                            break;
                        }
                    }
                }

                $result = [
                    'status' => 1,
                    'message' => 'Данные успешно получены!',
                    'data' => $data,
                ];
                return json_encode($result);
            }
            else {
                $result = [
                    'status' => -1,
                    'message' => 'Неверный домен сайта!',
                    'data' => [],
                ];
                return json_encode($result);
            }
        } else {
            $result = [
                'status' => -1,
                'message' => 'Неверно составлен ajax-запрос!',
                'data' => [],
            ];
            return json_encode($result);
        }
    }
}
