$(document).ready(function(){

    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var recipient = button.data('whatever')
        var modal = $(this)
        modal.find('.modal-title').text('Оценка сайта ' + recipient)
        modal.find('.progressBlock').text('Процент оптимизации сайта ')
        modal.find('.modal-body input').val(recipient)
    })
})
