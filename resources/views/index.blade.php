@extends('layouts.layout')

@section('content')
    <div class="container">
        <a class="navbar-brand " href="#">SeoGod</a>
    </div>

    <header class="header" id="header">
        <div class="overlay">
            <div class="container">
                <h1>Анализ сайта онлайн</h1>
                <p class="lead">Проверка сайта и мониторинг изменений. Инструмент для продвижения в поисковых системах.</p>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Введите домен.." id="domain-input">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button" id="results-button">Анализировать</button>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="main mb-5">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h2 class="mb-3">Возможности сервиса</h2>
                <p class="lead">Пользователи сервиса смогут улучшить SEO для своих сайтов и усилить позиции в поисковой выдаче благодаря полученным рекомендациям, что позволит привлечь на сайт новых клиентов для пользователя.
                    Главной целью сервиса является увеличение конверсий, поискового трафика и продаж на сайтах.</p>
            </div>
        </div>
        <div class="container">
            <h3 class="mb-4 text-center text-md-left">Профит при внедрении рекомендаций:</h3>
            <div class="row">
                <div class="col-md-3 col-sm-6 justify-content-center d-flex">
                    <div class="card border-greensea mb-3 ">

                        <div class="card-body text-greensea">
                            <h5 class="card-title text-center">
                                <svg width="5em" height="5em" viewBox="0 0 16 16" class="bi bi-diagram-3" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M6 3.5A1.5 1.5 0 0 1 7.5 2h1A1.5 1.5 0 0 1 10 3.5v1A1.5 1.5 0 0 1 8.5 6v1H14a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-1 0V8h-5v.5a.5.5 0 0 1-1 0V8h-5v.5a.5.5 0 0 1-1 0v-1A.5.5 0 0 1 2 7h5.5V6A1.5 1.5 0 0 1 6 4.5v-1zM8.5 5a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1zM0 11.5A1.5 1.5 0 0 1 1.5 10h1A1.5 1.5 0 0 1 4 11.5v1A1.5 1.5 0 0 1 2.5 14h-1A1.5 1.5 0 0 1 0 12.5v-1zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1zm4.5.5A1.5 1.5 0 0 1 7.5 10h1a1.5 1.5 0 0 1 1.5 1.5v1A1.5 1.5 0 0 1 8.5 14h-1A1.5 1.5 0 0 1 6 12.5v-1zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1zm4.5.5a1.5 1.5 0 0 1 1.5-1.5h1a1.5 1.5 0 0 1 1.5 1.5v1a1.5 1.5 0 0 1-1.5 1.5h-1a1.5 1.5 0 0 1-1.5-1.5v-1zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1z"/>
                                </svg>
                            </h5>
                            <p class="card-text">Оптимизация коммерческих факторов ранжирования</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 justify-content-center d-flex">
                    <div class="card border-greensea mb-3 col-md-4 col-sm-12">

                        <div class="card-body text-greensea">
                            <h5 class="card-title text-center">
                                <svg width="5em" height="5em" viewBox="0 0 16 16" class="bi bi-award" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M9.669.864L8 0 6.331.864l-1.858.282-.842 1.68-1.337 1.32L2.6 6l-.306 1.854 1.337 1.32.842 1.68 1.858.282L8 12l1.669-.864 1.858-.282.842-1.68 1.337-1.32L13.4 6l.306-1.854-1.337-1.32-.842-1.68L9.669.864zm1.196 1.193l-1.51-.229L8 1.126l-1.355.702-1.51.229-.684 1.365-1.086 1.072L3.614 6l-.25 1.506 1.087 1.072.684 1.365 1.51.229L8 10.874l1.356-.702 1.509-.229.684-1.365 1.086-1.072L12.387 6l.248-1.506-1.086-1.072-.684-1.365z"/><path d="M4 11.794V16l4-1 4 1v-4.206l-2.018.306L8 13.126 6.018 12.1 4 11.794z"/>
                                </svg>
                            </h5>
                            <p class="card-text">Усиление позиций сайта в поисковой выдаче</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 justify-content-center d-flex">
                    <div class="card border-greensea mb-3 col-md-4 col-sm-12">
                        <div class="card-body text-greensea">
                            <h5 class="card-title text-center">
                                <svg width="5em" height="5em" viewBox="0 0 16 16" class="bi bi-bar-chart" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M4 11H2v3h2v-3zm5-4H7v7h2V7zm5-5h-2v12h2V2zm-2-1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h2a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1h-2zM6 7a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v7a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1V7zm-5 4a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1v-3z"/>
                                </svg>
                            </h5>
                            <p class="card-text">Рост посещаемости вашего сайта</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 justify-content-center d-flex">
                    <div class="card border-greensea mb-3 col-md-4 col-sm-12">
                        <div class="card-body text-greensea">
                            <h5 class="card-title text-center">
                                <svg width="5em" height="5em" viewBox="0 0 16 16" class="bi bi-border-style" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 3.5a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-1zm0 4a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-5a.5.5 0 0 1-.5-.5v-1zm0 4a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm8 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm-4 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm8 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1zm-4-4a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-5a.5.5 0 0 1-.5-.5v-1z"/>
                                </svg>
                            </h5>
                            <p class="card-text">Увеличение конверсий за счет проработки контактной инфы</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="bg-greensea">
        <div class="container">
            <span class="text-light">©2020 Company Феникс Team: Sergey, Artem, Sanya, Evelina </span>
        </div>
    </footer>

    @include('components._modal')

@endsection
