document.addEventListener('DOMContentLoaded', function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#results-button').on('click', function () {
        let domain = $('#domain-input').val();
        domain = domain.toLowerCase();
        domain = domain.replace(/(https|http|:|\/\/|\s)/g, '');
        domain = domain.split('/')[0];
        if ( domain.match(/[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,9}/) ) {
            $('#results-modal').modal('show');
            $.ajax({
                method: 'post',
                url: '/',
                data: {
                    domain: domain
                },
                dataType: 'json',
                beforeSend: function () {
                    $('.loading-overlay').removeClass('d-none');
                },
                success: function (res) {
                    if ( res.status == -1 ) {
                        alert(res.message);
                        $('#results-modal').modal('hide');
                    } else {
                        console.log(res);
                        $('#results-modal .modal-content .modal-header h4').html('Оценка сайта: ' + domain);
                        $('.loading-overlay').addClass('d-none');

                        let progressPercent = 100 / 26;
                        let progress = 0;
                        let data = res.data;
                        let phoneData = $('#phone-data'),
                            phoneClickableData = $('#phone-clickable-data'),
                            emailData = $('#email-data'),
                            emailClickableData = $('#email-clickable-data'),
                            workTimeData = $('#work-time-data'),
                            addressData = $('#address-data'),
                            titleData = $('#title-data'),
                            keywordsData = $('#keywords-data'),
                            descriptionData = $('#description-data'),
                            h1Data = $('#h1-data'),
                            h2Data = $('#h2-data'),
                            h3Data = $('#h3-data'),
                            h4Data = $('#h4-data'),
                            h5Data = $('#h5-data'),
                            h6Data = $('#h6-data'),
                            paymentData = $('#payment-data'),
                            warrantyData = $('#warranty-data'),
                            aboutCompanyData = $('#about_company-data'),
                            certificatesData = $('#certificates-data'),
                            contactsData = $('#contacts-data'),
                            deliveryData = $('#delivery-data'),
                            promotionsData = $('#promotions-data'),
                            pricesData = $('#prices-data'),
                            reviewsData = $('#reviews-data'),
                            returnData = $('#return-data'),
                            CPUData = $('#cpu-data'),
                            fixItList = $('#fix-it-list');
                        let correctSVG = "<path fill-rule=\"evenodd\" d=\"M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z\"/>",
                            wrongSVG = "<path fill-rule=\"evenodd\" d=\"M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z\"/><path fill-rule=\"evenodd\" d=\"M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z\"/>";
                        let fixItMessages = [];

                        let parent;
                        parent = phoneData.closest('div');
                        if (data.company_info.phone.length > 0) {
                            phoneData.html(data.company_info.phone);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                                                        progress++;
                        } else {
                            phoneData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить контактные данные: Телефон');
                        }

                        parent = phoneClickableData.closest('div')
                        if (data.company_info.clickable_phone) {
                            phoneClickableData.html('С ссылкой');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                                                        progress++;
                        } else {
                            phoneClickableData.html('Без ссылки');
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить ссылку на контактные данные: Телефон');
                        }

                        parent = emailData.closest('div');
                        if (data.company_info.email.length > 0) {
                            emailData.html(data.company_info.email);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                                                        progress++;
                        } else {
                            emailData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить контактные данные: Почта');
                        }

                        parent = emailClickableData.closest('div')
                        if (data.company_info.clickable_email) {
                            emailClickableData.html('С ссылкой');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                                                        progress++;
                        } else {
                            emailClickableData.html('Без ссылки');
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить ссылку на контактные данные: Почта');
                        }

                        parent = workTimeData.closest('div');
                        if (data.company_info.work_time.length > 0) {
                            workTimeData.html(data.company_info.work_time);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                                                        progress++;
                        } else {
                            workTimeData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Разместить информацию: Режим работы');
                        }

                        parent = addressData.closest('div');
                        if (data.company_info.address.length > 0) {
                            addressData.html(data.company_info.address);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            addressData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Разместить контактные данные: Адрес');
                        }

                        parent = titleData.closest('div');
                        if (data.meta.title.length > 0) {
                            titleData.html(data.meta.title);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            titleData.html("Отсутствие, либо недостаточное содержание");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Расширить значение title');
                        }

                        parent = keywordsData.closest('div');
                        if (data.meta.meta_tags.keywords && data.meta.meta_tags.keywords.replace(/\s/g, '').split(',').length > 0) {
                            keywordsData.html(data.meta.meta_tags.keywords);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            keywordsData.html("Отсутствие, либо недостаточное содержание");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Расширить значение keywords');
                        }

                        parent = descriptionData.closest('div');
                        if (data.meta.meta_tags.description && data.meta.meta_tags.description.length > 0) {
                            descriptionData.html(data.meta.meta_tags.description);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            descriptionData.html("Отсутствие, либо недостаточное содержание");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Расширить значение description');
                        }

                        parent = h1Data.closest('div');
                        if (data.headings.h1 > 0) {
                            h1Data.html(data.headings.h1);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            h1Data.html("0");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить теги h1');
                        }

                        parent = h2Data.closest('div');
                        if (data.headings.h2 > 0) {
                            h2Data.html(data.headings.h2);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            h2Data.html("0");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить теги h2');
                        }

                        parent = h3Data.closest('div');
                        if (data.headings.h3 > 0) {
                            h3Data.html(data.headings.h3);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            h3Data.html("0");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить теги h3');
                        }

                        parent = h4Data.closest('div');
                        if (data.headings.h4 > 0) {
                            h4Data.html(data.headings.h4);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            h4Data.html("0");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить теги h4');
                        }

                        parent = h5Data.closest('div');
                        if (data.headings.h5 > 0) {
                            h5Data.html(data.headings.h5);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            h5Data.html("0");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить теги h5');
                        }

                        parent = h6Data.closest('div');
                        if (data.headings.h6 > 0) {
                            h6Data.html(data.headings.h6);
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            h6Data.html("0");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить теги h6');
                        }

                        parent = paymentData.closest('div');
                        if (data.pages.payment) {
                            paymentData.html('Присутствует');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            paymentData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить страницу Оплата');
                        }

                        parent = warrantyData.closest('div');
                        if (data.pages.warranty) {
                            warrantyData.html('Присутствует');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            warrantyData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить страницу Гарантии');
                        }

                        parent = reviewsData.closest('div');
                        if (data.pages.reviews) {
                            reviewsData.html('Присутствует');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            reviewsData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить страницу Отзывы');
                        }

                        parent = aboutCompanyData.closest('div');
                        if (data.pages.about_company) {
                            aboutCompanyData.html('Присутствует');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            aboutCompanyData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить страницу О компании');
                        }

                        parent = certificatesData.closest('div');
                        if (data.pages.certificates) {
                            certificatesData.html('Присутствует');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            certificatesData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить страницу Сертфикаты');
                        }

                        parent = deliveryData.closest('div');
                        if (data.pages.delivery) {
                            deliveryData.html('Присутствует');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            deliveryData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить страницу Доставка');
                        }

                        parent = pricesData.closest('div');
                        if (data.pages.prices) {
                            pricesData.html('Присутствует');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            pricesData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить страницу Цены');
                        }

                        parent = promotionsData.closest('div');
                        if (data.pages.promotions) {
                            promotionsData.html('Присутствует');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            promotionsData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить страницу Акции');
                        }

                        parent = returnData.closest('div');
                        if (data.pages.return) {
                            returnData.html('Присутствует');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            returnData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить страницу Возврат');
                        }

                        parent = contactsData.closest('div');
                        if (data.pages.contacts) {
                            contactsData.html('Присутствует');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            contactsData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить страницу Контакты');
                        }

                        parent = CPUData.closest('div');
                        if (data.beautiful_routing) {
                            CPUData.html('Присутствует');
                            parent.removeClass('border-rednsea-site').removeClass('text-rednsea').addClass('border-greensea-site').addClass('text-greensea');
                            parent.find('svg').html(correctSVG);
                            progress++;
                        } else {
                            CPUData.html("Отсутствует");
                            parent.addClass('border-rednsea-site').addClass('text-rednsea').removeClass('border-greensea-site').removeClass('text-greensea');
                            parent.find('svg').html(wrongSVG);
                            fixItMessages.push('Добавить ЧПУ');
                        }

                        progress = Math.round(progressPercent * progress);
                        $('.progress-bar[role=progressbar]').css({'width': progress+'%'});
                        $('.progress-bar[role=progressbar]').html(progress+'%');
                        console.log(progress);

                        fixItList.html('');
                        for (let i = 0; i < fixItMessages.length; i++) {
                            fixItList.append(`<li class="list-group-item">${fixItMessages[i]}</li>`);
                        }
                    }
                }
            })
        } else {
            alert("Введен неверный домен сайта!");
        }
        console.log(domain);
    })

});
